Introduction
============

In this chapter, we will provide low Level API's which can be used to develop software that is compatible with BitGuard.
BitGuard uses connectivity over USB or BLUETOOTH, In this section we will describe API's using BLUETOOTH communication channel. Same API's can be used for USB. The type of protocol used between device and BitGuard is Universal Asynchronous Serial Communication.  
First we will describe how to initialize the device and confirming the integrity of BitGuard

Overview
--------

**Commands Brief Description**

======== ======================== =========================================== 	
Command   Name					   Description
======== ======================== ===========================================
**0x00**  Initialize               Used to initialize BitGuard 
**0x01**  Get Address			   Used to get public Address of the coin	
**0x02**  Get Public Key		   Used to get public key of a coin	
**0x03**  Initialize transaction   Used to initialize transaction signing
**0xff**  Reset 				   Used to terminate a process
======== ======================== ===========================================

Above mentioned commands will be in HEX.

**Buffer Sizes**

======== ==============
  Mode	  Buffer Size
======== ==============
  BLE 		20 Bytes
  USB		1023 Bytes
======== ============== 

**Command Format**

Commands mentioned in table above will be in HEX (1 byte). Commands other than these commands will have **start byte as '<'** (ASCII : 60) and **stop byte as '>'** (ASCII : 62).

.. note::
	These API's are meant to be used only by developers and not users. Using them without permission of Blockwala will void the warranty of the device. 