Get Public Key
==============

Once, Bitguard hardware is initialized then you can get public key of a coin from bitGuard. Follow the following steps to get public address. 

Initialize Public Key Request 
-----------------------------

	========= ==================================================== ================================================== =======================
	Command	   Procedure				                           Device Side Process	    						   Response 
	========= ==================================================== ================================================== =======================
	**0x02**  Send 0x02 to initialize public key process    	    Not required   
																													   * type    : "pub_key" 
																													   * status  : true
																													   
																													   * req     : "ticker"
	========= ==================================================== ================================================== =======================

	 **Response JSON Object:**   	
		* **type(string)** : Type of procedure.
		* **status(boolean)** : Gives the success or failure status.
		* **req(string)**   : Gives type of request data in this case it is "ticker" of the coin.

	  **Example response**:

	    .. sourcecode:: js

	    	{
	    	  type 	: "pub_key",
	    	  status : true,
	    	  req    : "ticker"
	    	}


Coin Selection
--------------

The public key you will get in this API will be in uncompressed HEX form. For further information on different types of keys refer `here  <https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch04.asciidoc>`_ .

	========= ==================================================== ================================================== =======================
	Command	   Procedure				                           Device Side Process	    						   Response 
	========= ==================================================== ================================================== =======================
	**<BTC>** Send ticker to get the public address of that coin.
			  				                                         Not required                                       
			  																										   * coin    : "BTC" 
																													   * status  : true
																													   * pub_key : string
																													   * type    : "pub_key"			 				                           
	========= ==================================================== ================================================== =======================

	  **Response JSON Object:**   	
	    * **type(string)** : Type of procedure.
	    * **coin(string)**   : Gives the ticker of the coin being queried.
	    * **pub_key(string)** :Gives the public key in string format.
	    * **status(boolean)** : Gives the success or failure status.

		**Example response**:

		    .. sourcecode:: js

		    	{
		    	  coin 	: "BTC",
		    	  pub_key : "b7c4a254c8c678868ff2cdf42f1af1a97c42a0c150d0e10374a9353a98a3bc86b7c4a254c8c678868ff2cdf42f1af1a97c42a0c150d0e10374a9353a98a3bc86",
		    	  status  : true,
		    	  type    : "pub_key" 
		    	}