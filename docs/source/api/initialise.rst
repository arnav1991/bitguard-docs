Initialization
==============

After your device is connected follow the following steps to verify the integrity of BitGuard Hardware and initialize it.


========= ==================================================== ================================================== ====================
Command	   Procedure				                           Device Side Process	    						   Response 
========= ==================================================== ================================================== ====================
**0x00**  Once connected send 0x00 to initialize the BitGuard	Enter pin in BitGuard and press B1 to initialize   
																												   * version : string 
																												   * status  : boolean
																												   
																												   * ack     : boolean
																			 			                           
========= ==================================================== ================================================== ====================

**Response JSON Object:**   	
	* **Version(string)** : It is the version of the firmware in Bitguard
	* **Status(boolean)** : true : Wallet is created , false : New Wallet
	* **Ack(bloolean)**   : true : acknowledgment is required false : acknowledgment is not required.


**Example Response**:

    .. sourcecode:: js

    	{
    	version : "v0.14",
    	status  : true,
    	ack     : false
    	}


