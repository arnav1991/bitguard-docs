Supported Tokens
================

This is the list of coins supported with there ticker.

======= =========== ================
TICKER   NAME		 PATH
======= =========== ================
BTC      Bitcoin     m/44'/0'/0'/0
LTC      Litecoin    m/44'/2'/0'/0
DASH     Dash        m/44'/5'/0'/0
ETH      Ethereum    m/44'/60'/0'/0
DOGE     Dogecoin    m/44'/3'/0'/0
======= =========== ================

.. note::
	For getting support for your coin in BitGuard Contact us at info@blockwala.io 