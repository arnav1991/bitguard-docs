Introduction
============

In this chapter, we will give specifications of our live hardware wallets : BitGuard Flex and BitGuard Iron. Flex is for daily usage and frequent payments. It has bluetooth support and colored screen. Iron is for long term storage of crypto currencies and it only supports communication over **c-type USB**. In next sub chapters we will describe both the models in detail. 