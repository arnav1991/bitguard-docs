.. BitGuard documentation master file, created by
   sphinx-quickstart on Sat Aug 11 19:24:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BitGuard Developer's Guide 
==========================

.. note::
   This documentation is currently under development. 
   This documentation is intended for developers only and not general users.

The BitGuard is a HD hardware wallet for crypto currencies. This guide will guide you through the low level API's for directly connecting to BitGuard. 


.. toctree::
   :caption: Background Information
   :maxdepth: 1

   background/introduction
   background/root_seed
   background/HD_wallets

.. toctree::
   :caption: Wallets
   :maxdepth: 1

   wallets/introduction.rst
   wallets/flex.rst
   wallets/iron.rst
   wallets/supported_coins.rst

.. toctree::
   :caption: Communication
   :maxdepth: 1

   communication/introduction.rst
   communication/Bluetooth.rst
   communication/USB.rst

.. toctree::
   :caption: Low Level Api's
   :maxdepth: 1

   api/introduction
   api/pwr_on
   api/initialise
   api/get_public_address
   api/get_public_key
   api/txn_signing
   api/reset
   api/error_cases







