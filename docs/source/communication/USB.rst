Universal Serial Bus
====================

.. figure:: image/usb.png
   :align: center

Overview
--------

BitGuard uses c-type USB (Universal Serial Bus) port for communication and charging of the BitGuard. The version of USB used is 2.1. We choose c-type to support maximum devices with BitGuard. USB communication is meant to be used only with android and iOS. Communication with smart phones is only supported through BLE to avoid use of OTG cable. Firmware update will be done using USB c-type only through chrome application. The USB device mode supports 8 endpoint addresses. All endpoint addresses have one input and one output endpoint, for a total of 16 endpoints. BitGuard uses bulk transfer mode. The USB host mode supports up to 8 pipes. The maximum data payload size is selectable up to **1023 bytes**.

Features
--------

 Compatible with the USB 2.1 specification

	* USB Embedded Device mode

	* Supports full (12Mbit/s) and low (1.5Mbit/s) speed communication

	* Supports Link Power Management (LPM-L1) protocol

	* On-chip transceivers with built-in pull-ups and pull-downs

	* On-Chip USB serial resistors

	* Supports 8 IN endpoints and 8 OUT endpoints

	* No endpoint size limitations

	* Supports feedback endpoint

	* Supports crystal less clock




