Introduction
============

In this chapter, we'll first provide some background information about hierarchical deterministic wallets. Then, we'll introduce how it is implimented in BitGuard. For in depth study on how wallets are created for multiple crypto currencies refer `here <https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch05.asciidoc>`_.

